#include "assembler.hpp"
#include "heatProblem.hpp"
#include <iostream>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include "Eigen/Eigen/Eigen"
#include"Eigen/Eigen/Dense"
#include "Eigen/Eigen/Sparse"

using namespace std;
using namespace Eigen;

HeatAssembler::HeatAssembler(Grid* g, HeatProblem* pb) {

  _grid = g;
  _pb   = pb;
  mat1.resize(8*_grid->getNbPoints(0)*_grid->getNbPoints(1) + 10* _grid->Get_nbsigne());
  mat2.resize(8*_grid->getNbPoints(0)*_grid->getNbPoints(1) + 10* _grid->Get_nbsigne());
  mat3.resize(8*_grid->getNbPoints(0)*_grid->getNbPoints(1) + 10* _grid->Get_nbsigne());
  Im.resize(_grid->getNbPoints(2));
  Um.resize(_grid->getNbPoints(2));
  Nx = _grid->getNbPoints(0);
  Ny = _grid->getNbPoints(1);
  Ne = _grid->getNbPoints(2);
  //k  = 1;
  dx = _grid->getSpacing(0);
  dy = _grid->getSpacing(1);


  _x.resize(Nx); // (xmin+h, ..., xmax-h)
  for (int i = 0 ; i < Nx ; ++i)
  _x[i] =  ((i+1)-0.5)*dx -1.;

  _y.resize(Ny);
  for (int i = 0 ; i < Ny ; ++i)
  _y[i] =  ((i+1)-0.5)*dy -1.;

}

void HeatAssembler::fluxx(int i, int j){

  pi = acos(-1.);

  fix=0;
  fiy=0;

  //calcul normale sur le point d'interface
  fix = (_grid->get_u(i,j)*(_x[i+1]-_grid->get_xinter(i,j))-_grid->get_u(i+1,j)*(_x[i]-_grid->get_xinter(i,j)))/dx;
  fiy = (_grid->get_v(i,j)*(_x[i+1]-_grid->get_xinter(i,j))-_grid->get_v(i+1,j)*(_x[i]-_grid->get_xinter(i,j)))/dx;
  fiix = fix/sqrt(pow(fix,2)+pow(fiy,2));
  fiiy = fiy/sqrt(pow(fix,2)+pow(fiy,2));
  fix = fiix;
  fiy = fiiy;

  marqueur = 0;
  if (_grid->Levelset(i,j) < 0.) {
    // je calcule le flux � gauche:
    marqueur = 2;

    // je choisis les points suivant la direction de la normale pour calculer la derivee normale
    xj = _grid->get_xinter(i,j);
    yj = _y[j];

    d = _grid->get_xinter(i,j) - _x[i];
    r2 = sqrt(pow((_grid->get_xinter(i,j)-0),2) + pow((_y[j]-0),2));
    the = acos( (_grid->get_xinter(i,j)-0)/r2);
    if (_y[j] <= 0.) the = 2.*pi-the;
    dd = -d*tan(the);


    if ((dd < 0.) && (dd >= -dy)) {
      indk = _grid->ind(i,j);
      xk = _x[i];
      yk = _y[j];
      indi = _grid->ind(i,j-1);
      xi = _x[i];
      yi = _y[j-1];
      if (_grid->Levelset(i,j)*_grid->Levelset(i,j-1) <= 0.) {marqueur = 1;}
    }

    if (dd < -dy){
      indk = _grid->ind(i+1,j-1);
      xk = _x[i+1];
      yk =  _y[j-1];
      indi = _grid->ind(i,j-1);
      xi = _x[i];
      yi = _y[j-1];
      if (_grid->Levelset(i,j)*_grid->Levelset(i,j-1) <= 0.) {marqueur = 1;}
      if (_grid->Levelset(i+1,j-1)*_grid->Levelset(i,j-1) <= 0.) {marqueur = 1;}
    }

    if ((dd >= 0.) && (dd <= dy)){
      indk = _grid->ind(i,j);
      xk = _x[i];
      yk = _y[j];
      indi = _grid->ind(i,j+1);
      xi = _x[i];
      yi = _y[j+1];
      if (_grid->Levelset(i,j)*_grid->Levelset(i,j+1)<= 0) {marqueur = 1;}
    }

    if (dd > dy){
      indk = _grid->ind(i+1,j+1);
      xk = _x[i+1];
      yk = _y[j+1];
      indi = _grid->ind(i,j+1);
      xi = _x[i];
      yi = _y[j+1];
      if (_grid->Levelset(i,j)*_grid->Levelset(i,j+1) <= 0) {marqueur = 1;}
      if (_grid->Levelset(i+1,j+1)*_grid->Levelset(i,j+1) <= 0) {marqueur = 1;}
    }

    if (marqueur != 1) {
      // formule ordre 1 non-degeneree, dans la direction de la normale avec les 3 points choisis ci-dessus

      // calcul des coef du gradient (interpolation lineaire sur les 3 points)
      denom = (xj-xk)*(yj-yi)-(xj-xi)*(yj-yk);
      alphaj = (yk-yi)/denom;
      betaj = (xi-xk)/denom;

      denom = (xi-xk)*(yi-yj)-(xi-xj)*(yi-yk);
      alphai = (yk-yj)/denom;
      betai = (xj-xk)/denom;

      denom = (xk-xj)*(yk-yi)-(xk-xi)*(yk-yj);
      alphak = (yj-yi)/denom;
      betak = (xi-xj)/denom;

      // indk
      mat1[m] = _grid->get_tx2(i,j)-1;
      mat2[m] = indk-1;
      mat3[m] = _grid->coef(_grid->get_xinter(i,j),_y[j])*(alphak*fix + betak*fiy);
      //std::cout<<setprecision(19) << mat2[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
      //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      m = m +1;

      // indi
      mat1[m] = _grid->get_tx2(i,j)-1;
      mat2[m] = indi-1;
      mat3[m] = _grid->coef(_grid->get_xinter(i,j),_y[j])*(alphai*fix + betai*fiy);
      //std::cout<<setprecision(19) << mat2[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
      //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      m = m +1;

      // int(i,i+1),j
      mat1[m] = _grid->get_tx2(i,j)-1;
      mat2[m] = _grid->get_tx2(i,j)-1;
      mat3[m] = _grid->coef(_grid->get_xinter(i,j),_y[j])*(alphaj*fix + betaj*fiy);
      //std::cout<<setprecision(19) << mat2[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
      //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      m = m +1;

    } else {
      // on fait schema degenere: : derivee ordre 1 dans la direction x

      // ind(i,j)
      mat1[m] =_grid->get_tx2(i,j)-1;
      mat2[m] = _grid->ind(i,j)-1;
      if (fix>=0) {
        mat3[m] =  abs(-_grid->coef(_grid->get_xinter(i,j),_y[j])/(_grid->get_xinter(i,j)-_x[i]));
      }else{
        mat3[m] =  -abs(-_grid->coef(_grid->get_xinter(i,j),_y[j])/(_grid->get_xinter(i,j)-_x[i]));
      }
      //std::cout<<setprecision(19) << mat2[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
      //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      m = m +1;

      // int(i,i+1),j
      mat1[m] = _grid->get_tx2(i,j)-1;
      mat2[m] =  _grid->get_tx2(i,j)-1;
      if (fix>=0) {
        mat3[m] =  abs(-_grid->coef(_grid->get_xinter(i,j),_y[j])/(_grid->get_xinter(i,j)-_x[i]));
      }else{
        mat3[m] =  -abs(-_grid->coef(_grid->get_xinter(i,j),_y[j])/(_grid->get_xinter(i,j)-_x[i]));
      }
      //std::cout<<setprecision(19) << mat2[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
      //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      m = m +1;

    }
  }else {
    // flux � droite
    // je choisis les points suivant la direction de la normale pour calculer la derivee normale
    marqueur = 2;

    xj = _grid->get_xinter(i,j);
    yj = _y[j];

    d = dx-(_grid->get_xinter(i,j) - _x[i]);
    r2 = sqrt(pow((_grid->get_xinter(i,j)-0),2) + pow((_y[j]-0),2));
    the = acos( (_grid->get_xinter(i,j)-0)/r2);
    if (_y[j] <= 0.) the = 2.*pi-the;
    dd = d*tan(the);

    if ((dd >= 0.) && (dd <= dy)) {
      indk = _grid->ind(i+1,j);
      xk = _x[i+1];
      yk = _y[j];
      indi = _grid->ind(i+1,j+1);
      xi = _x[i+1];
      yi = _y[j+1];
      if (_grid->Levelset(i+1,j)*_grid->Levelset(i+1,j+1) <= 0.)  {marqueur = 1;}
    }


    if (dd > dy){
      indk = _grid->ind(i,j+1);
      xk = _x[i];
      yk =  _y[j+1];
      indi = _grid->ind(i+1,j+1);
      xi = _x[i+1];
      yi = _y[j+1];
      if (_grid->Levelset(i+1,j+1)*_grid->Levelset(i,j+1) <= 0.)  {marqueur = 1;}
      if (_grid->Levelset(i+1,j)*_grid->Levelset(i+1,j+1) <= 0.)  {marqueur = 1;}
    }

    if ((dd < 0.) && (dd >= -dy)){
      indk = _grid->ind(i+1,j);
      xk = _x[i+1];
      yk = _y[j];
      indi = _grid->ind(i+1,j-1);
      xi = _x[i+1];
      yi = _y[j-1];
      if (_grid->Levelset(i+1,j)*_grid->Levelset(i+1,j-1) <= 0)  {marqueur = 1;}
    }

    if ((dd < -dy)){
      indk = _grid->ind(i+1,j-1);
      xk = _x[i+1];
      yk = _y[j-1];
      indi = _grid->ind(i,j-1);
      xi = _x[i];
      yi = _y[j-1];
      if (_grid->Levelset(i+1,j)*_grid->Levelset(i+1,j-1) <= 0) {marqueur = 1;}
      if (_grid->Levelset(i+1,j-1)*_grid->Levelset(i,j-1) <= 0)  {marqueur = 1;}
    }

    if (marqueur != 1) {
      // calcul des coef du gradient  (interpolation lineaire sur les 3 points)
      denom = (xj-xk)*(yj-yi)-(xj-xi)*(yj-yk);
      alphaj = (yk-yi)/denom;
      betaj = (xi-xk)/denom;


      denom = (xi-xk)*(yi-yj)-(xi-xj)*(yi-yk);
      alphai = (yk-yj)/denom;
      betai = (xj-xk)/denom;

      denom = (xk-xj)*(yk-yi)-(xk-xi)*(yk-yj);
      alphak = (yj-yi)/denom;
      betak = (xi-xj)/denom;

      // indk
      mat1[m] = _grid->get_tx2(i,j)-1;
      mat2[m] = _grid->get_tx2(i,j)-1;
      mat3[m] = _grid->coef(_grid->get_xinter(i,j),_y[j])*(alphaj*fix + betaj*fiy);
      //std::cout<<setprecision(19) << mat2[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
      //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      m = m +1;

      // indi
      mat1[m] = _grid->get_tx2(i,j)-1;
      mat2[m] = indk-1;
      mat3[m] = _grid->coef(_grid->get_xinter(i,j),_y[j])*(alphak*fix + betak*fiy);//probleme de signe de betak
      //std::cout<<setprecision(19) << mat2[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
      //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      m = m +1;

      // int(i,i+1),j
      mat1[m] = _grid->get_tx2(i,j)-1;
      mat2[m] = indi-1;
      mat3[m] = _grid->coef(_grid->get_xinter(i,j),_y[j])*(alphai*fix + betai*fiy);
      //std::cout<<setprecision(19) << mat2[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
      //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      m = m +1;

    }else{
      // on fait schema degenere: : derivee ordre 1 dans la direction x

      // ind(i,j)
      mat1[m] =_grid->get_tx2(i,j)-1;
      mat2[m] =_grid->get_tx2(i,j)-1;
      if (fix>=0) {
        mat3[m] =  abs(-_grid->coef(_grid->get_xinter(i,j),_y[j])/(_x[i+1]-_grid->get_xinter(i,j)));
      }else{
        mat3[m] =  -abs(-_grid->coef(_grid->get_xinter(i,j),_y[j])/(_x[i+1]-_grid->get_xinter(i,j)));
      }
      //std::cout<<setprecision(19) << mat2[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
      //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      m = m +1;

      // int(i,i+1),j
      mat1[m] = _grid->get_tx2(i,j)-1;
      mat2[m] = _grid->ind(i+1,j)-1;
      if (fix>=0) {
        mat3[m] =  abs(_grid->coef(_grid->get_xinter(i,j),_y[j])/(_x[i+1]-_grid->get_xinter(i,j)));
      }else{
        mat3[m] =  -abs(_grid->coef(_grid->get_xinter(i,j),_y[j])/(_x[i+1]-_grid->get_xinter(i,j)));
      }
      //std::cout<<setprecision(19) << mat2[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
      //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      m = m +1;

    }
  }

}


void HeatAssembler::fluxy(int i, int j){


  pi = acos(-1.);

  fix=0;
  fiy=0;

  //calcul normale sur le point d'interface
  fix = (_grid->get_u(i,j)*(_y[j+1]-_grid->get_yinter(i,j))-_grid->get_u(i,j+1)*(_y[j]-_grid->get_yinter(i,j)))/dy;
  fiy = (_grid->get_v(i,j)*(_y[j+1]-_grid->get_yinter(i,j))-_grid->get_v(i,j+1)*(_y[j]-_grid->get_yinter(i,j)))/dy;
  fiix = fix/sqrt(pow(fix,2)+pow(fiy,2));
  fiiy = fiy/sqrt(pow(fix,2)+pow(fiy,2));
  fix = fiix;
  fiy = fiiy;

  marqueur = 0;
  if (_grid->Levelset(i,j) < 0.) {
    // je calcule le flux � gauche:
    marqueur = 2;

    // je choisis les points suivant la direction de la normale pour calculer la derivee normale
    xj = _x[i];
    yj = _grid->get_yinter(i,j);

    d = dy-(_grid->get_yinter(i,j) - _y[j]);
    r2 = sqrt(pow((_grid->get_yinter(i,j)-0),2) + pow((_x[i]-0),2));
    the = acos( -(_grid->get_yinter(i,j)-0)/r2);
    if (_x[i] <= 0.) the = 2.*pi-the;
    dd = d*tan(the);

    if ((dd < 0.) && (dd >= -dx)) {
      indk = _grid->ind(i,j);
      xk = _x[i];
      yk = _y[j];
      indi = _grid->ind(i-1,j);
      xi = _x[i-1];
      yi = _y[j];
      if (_grid->Levelset(i,j)*_grid->Levelset(i-1,j) <= 0.) {marqueur = 1;}

    }

    if (dd < -dx){
      indk = _grid->ind(i-1,j+1);
      xk = _x[i-1];
      yk =  _y[j+1];
      indi = _grid->ind(i-1,j);
      xi = _x[i-1];
      yi = _y[j];
      if (_grid->Levelset(i,j)*_grid->Levelset(i-1,j) <= 0.) {marqueur = 1;}
      if (_grid->Levelset(i-1,j+1)*_grid->Levelset(i-1,j) <= 0.) {marqueur = 1;}
    }

    if ((dd >= 0.) && (dd <= dx)){
      indk = _grid->ind(i,j);
      xk = _x[i];
      yk = _y[j];
      indi = _grid->ind(i+1,j);
      xi = _x[i+1];
      yi = _y[j];
      if (_grid->Levelset(i,j)*_grid->Levelset(i+1,j)<= 0) {marqueur = 1;}
    }

    if (dd > dx){
      indk = _grid->ind(i+1,j+1);
      xk = _x[i+1];
      yk = _y[j+1];
      indi = _grid->ind(i+1,j);
      xi = _x[i+1];
      yi = _y[j];
      if (_grid->Levelset(i,j)*_grid->Levelset(i+1,j) <= 0) {marqueur = 1;}
      if (_grid->Levelset(i+1,j+1)*_grid->Levelset(i+1,j) <= 0) {marqueur = 1;}
    }

    if (marqueur != 1) {
      // formule ordre 1 non-degeneree, dans la direction de la normale avec les 3 points choisis ci-dessus

      // calcul des coef du gradient (interpolation lineaire sur les 3 points)
      denom = (xj-xk)*(yj-yi)-(xj-xi)*(yj-yk);
      alphaj = (yk-yi)/denom;
      betaj = (xi-xk)/denom;

      denom = (xi-xk)*(yi-yj)-(xi-xj)*(yi-yk);
      alphai = (yk-yj)/denom;
      betai = (xj-xk)/denom;

      denom = (xk-xj)*(yk-yi)-(xk-xi)*(yk-yj);
      alphak = (yj-yi)/denom;
      betak = (xi-xj)/denom;

      // indk
      mat1[m] = _grid->get_ty2(i,j)-1;
      mat2[m] = indk-1;
      mat3[m] = _grid->coef(_x[i],_grid->get_yinter(i,j))*(alphak*fix + betak*fiy);
      //std::cout<<setprecision(19) << mat2[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
      //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      m = m +1;

      // indi
      mat1[m] = _grid->get_ty2(i,j)-1;
      mat2[m] = indi-1;
      mat3[m] = _grid->coef(_x[i],_grid->get_yinter(i,j))*(alphai*fix + betai*fiy);
      //std::cout<<setprecision(19) << mat2[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
      //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      m = m +1;

      // int(i,i+1),j
      mat1[m] = _grid->get_ty2(i,j)-1;
      mat2[m] = _grid->get_ty2(i,j)-1;
      mat3[m] = _grid->coef(_x[i],_grid->get_yinter(i,j))*(alphaj*fix + betaj*fiy);
      //std::cout<<setprecision(19) << mat2[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
      //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      m = m +1;

    } else {
      // on fait schema degenere: : derivee ordre 1 dans la direction x

      // ind(i,j)
      mat1[m] =_grid->get_ty2(i,j)-1;
      mat2[m] = _grid->ind(i,j)-1;
      if (fiy>=0) {
        mat3[m] = abs(-_grid->coef(_x[i],_grid->get_yinter(i,j))/(_grid->get_yinter(i,j)-_y[j]));
      }else{
        mat3[m] = -abs(-_grid->coef(_x[i],_grid->get_yinter(i,j))/(_grid->get_yinter(i,j)-_y[j]));
      }
      //std::cout<<setprecision(19) << mat2[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
      //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      m = m +1;

      // int(i,i+1),j
      mat1[m] = _grid->get_ty2(i,j)-1;
      mat2[m] =  _grid->get_ty2(i,j)-1;
      if (fiy>=0) {
        mat3[m] = abs(-_grid->coef(_x[i],_grid->get_yinter(i,j))/(_grid->get_yinter(i,j)-_y[j]));
      }else{
        mat3[m] = -abs(-_grid->coef(_x[i],_grid->get_yinter(i,j))/(_grid->get_yinter(i,j)-_y[j]));
      }
      //std::cout<<setprecision(19) << mat2[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
      //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      m = m +1;

    }
  }else {
    // flux � droite
    // je choisis les points suivant la direction de la normale pour calculer la derivee normale
    marqueur = 2;

    xj = _x[i];
    yj = _grid->get_yinter(i,j);


    d = (_grid->get_yinter(i,j) - _y[j]);
    r2 = sqrt(pow((_grid->get_yinter(i,j)-0),2) + pow((_x[i]-0),2));
    the = acos( -(_grid->get_yinter(i,j)-0)/r2);
    if (_x[i] <= 0.) the = 2.*pi-the;
    dd = -d*tan(the);


    if ((dd >= 0.) && (dd <= dx)) {
      indk = _grid->ind(i,j+1);
      xk = _x[i];
      yk = _y[j+1];
      indi = _grid->ind(i+1,j+1);
      xi = _x[i+1];
      yi = _y[j+1];
      if (_grid->Levelset(i,j+1)*_grid->Levelset(i+1,j+1) <= 0.)  {marqueur = 1;}
    }
    if (dd > dx){
      indk = _grid->ind(i+1,j);
      xk = _x[i+1];
      yk =  _y[j];
      indi = _grid->ind(i+1,j+1);
      xi = _x[i+1];
      yi = _y[j+1];
      if (_grid->Levelset(i+1,j+1)*_grid->Levelset(i+1,j) <= 0.)  {marqueur = 1;}
      if (_grid->Levelset(i,j+1)*_grid->Levelset(i+1,j+1) <= 0.)  {marqueur = 1;}
    }

    if ((dd < 0.) && (dd >= -dx)){
      indk = _grid->ind(i,j+1);
      xk = _x[i];
      yk = _y[j+1];
      indi = _grid->ind(i-1,j+1);
      xi = _x[i-1];
      yi = _y[j+1];
      if (_grid->Levelset(i,j+1)*_grid->Levelset(i-1,j+1) <= 0)  {marqueur = 1;}
    }

    if ((dd < -dx)){
      indk = _grid->ind(i-1,j+1);
      xk = _x[i-1];
      yk = _y[j+1];
      indi = _grid->ind(i-1,j);
      xi = _x[i-1];
      yi = _y[j];
      //cout<<xi<<";"<<yj<<endl;
      if (_grid->Levelset(i,j+1)*_grid->Levelset(i-1,j+1) <= 0) {marqueur = 1;}
      if (_grid->Levelset(i-1,j+1)*_grid->Levelset(i-1,j) <= 0)  {marqueur = 1;}
    }

    if (marqueur != 1) {
      // calcul des coef du gradient  (interpolation lineaire sur les 3 points)
      denom = (xj-xk)*(yj-yi)-(xj-xi)*(yj-yk);
      alphaj = (yk-yi)/denom;
      betaj = (xi-xk)/denom;


      denom = (xi-xk)*(yi-yj)-(xi-xj)*(yi-yk);
      alphai = (yk-yj)/denom;
      betai = (xj-xk)/denom;

      denom = (xk-xj)*(yk-yi)-(xk-xi)*(yk-yj);
      alphak = (yj-yi)/denom;
      betak = (xi-xj)/denom;


      // indk
      mat1[m] = _grid->get_ty2(i,j)-1;
      mat2[m] = _grid->get_ty2(i,j)-1;
      mat3[m] = _grid->coef(_x[i],_grid->get_yinter(i,j))*(alphaj*fix + betaj*fiy);
      //std::cout<<setprecision(19) << mat2[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
      //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      m = m +1;

      // indi
      mat1[m] = _grid->get_ty2(i,j)-1;
      mat2[m] = indk-1;
      mat3[m] = _grid->coef(_x[i],_grid->get_yinter(i,j))*(alphak*fix + betak*fiy);//probleme de signe de betak
      //std::cout<<setprecision(19) << mat2[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
      //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      m=m+1;


      // int(i,i+1),j
      mat1[m] = _grid->get_ty2(i,j)-1;
      mat2[m] = indi-1;
      mat3[m] = _grid->coef(_x[i],_grid->get_yinter(i,j))*(alphai*fix + betai*fiy);
      //std::cout<<setprecision(19) << mat2[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
      //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      m = m +1;

    }else{
      // on fait schema degenere: : derivee ordre 1 dans la direction x

      // ind(i,j)
      mat1[m] =_grid->get_ty2(i,j)-1;
      mat2[m] =_grid->get_ty2(i,j)-1;
      if (fiy>=0) {
        mat3[m] =  abs(-_grid->coef(_x[i],_grid->get_yinter(i,j))/(_y[j+1]-_grid->get_yinter(i,j)));
      }else{
        mat3[m] =  -abs(-_grid->coef(_x[i],_grid->get_yinter(i,j))/(_y[j+1]-_grid->get_yinter(i,j)));
      }
      //std::cout<<setprecision(19) << mat2[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
      //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      m = m +1;

      // int(i,i+1),j
      mat1[m] = _grid->get_ty2(i,j)-1;
      mat2[m] = _grid->ind(i,j+1)-1;
      if (fiy>=0) {
        mat3[m] = abs(_grid->coef(_x[i],_grid->get_yinter(i,j))/(_y[j+1]-_grid->get_yinter(i,j)));
      }else{
        mat3[m] = -abs(_grid->coef(_x[i],_grid->get_yinter(i,j))/(_y[j+1]-_grid->get_yinter(i,j)));
      }
      //std::cout<<setprecision(19) << mat2[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
      //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      m = m +1;

    }
  }

}

void HeatAssembler::electrodefluxx(int i, int j){

  pi = acos(-1.);

  fix=0;
  fiy=0;

  //calcul normale sur le point d'interface
  fix = (_grid->get_u(i,j)*(_x[i+1]-_grid->get_xinter(i,j))-_grid->get_u(i+1,j)*(_x[i]-_grid->get_xinter(i,j)))/dx;
  fiy = (_grid->get_v(i,j)*(_x[i+1]-_grid->get_xinter(i,j))-_grid->get_v(i+1,j)*(_x[i]-_grid->get_xinter(i,j)))/dx;
  fiix = fix/sqrt(pow(fix,2)+pow(fiy,2));
  fiiy = fiy/sqrt(pow(fix,2)+pow(fiy,2));
  fix = fiix;
  fiy = fiiy;

  marqueur = 0;
  if (_grid->Levelset(i,j) < 0.) {
    // je calcule le flux � gauche:
    marqueur = 2;

    // je choisis les points suivant la direction de la normale pour calculer la derivee normale
    xj = _grid->get_xinter(i,j);
    yj = _y[j];

    d = _grid->get_xinter(i,j) - _x[i];
    r2 = sqrt(pow((_grid->get_xinter(i,j)-0),2) + pow((_y[j]-0),2));
    the = acos( (_grid->get_xinter(i,j)-0)/r2);
    if (_y[j] <= 0.) the = 2.*pi-the;
    dd = -d*tan(the);
    //cout<<dd<<endl;

    if ((dd < 0.) && (dd >= -dy)) {
      indk = _grid->ind(i,j);
      xk = _x[i];
      yk = _y[j];
      indi = _grid->ind(i,j-1);
      xi = _x[i];
      yi = _y[j-1];
      if (_grid->Levelset(i,j)*_grid->Levelset(i,j-1) <= 0.) {marqueur = 1;}
    }

    if (dd < -dy){
      indk = _grid->ind(i+1,j-1);
      xk = _x[i+1];
      yk =  _y[j-1];
      indi = _grid->ind(i,j-1);
      xi = _x[i];
      yi = _y[j-1];
      if (_grid->Levelset(i,j)*_grid->Levelset(i,j-1) <= 0.) {marqueur = 1;}
      if (_grid->Levelset(i+1,j-1)*_grid->Levelset(i,j-1) <= 0.) {marqueur = 1;}
    }

    if ((dd >= 0.) && (dd <= dy)){
      indk = _grid->ind(i,j);
      xk = _x[i];
      yk = _y[j];
      indi = _grid->ind(i,j+1);
      xi = _x[i];
      yi = _y[j+1];
      if (_grid->Levelset(i,j)*_grid->Levelset(i,j+1)<= 0) {marqueur = 1;}
    }

    if (dd > dy){
      indk = _grid->ind(i+1,j+1);
      xk = _x[i+1];
      yk = _y[j+1];
      indi = _grid->ind(i,j+1);
      xi = _x[i];
      yi = _y[j+1];
      if (_grid->Levelset(i,j)*_grid->Levelset(i,j+1) <= 0) {marqueur = 1;}
      if (_grid->Levelset(i+1,j+1)*_grid->Levelset(i,j+1) <= 0) {marqueur = 1;}
    }

    if (marqueur != 1) {
      // formule ordre 1 non-degeneree, dans la direction de la normale avec les 3 points choisis ci-dessus

      // calcul des coef du gradient (interpolation lineaire sur les 3 points)
      denom = (xj-xk)*(yj-yi)-(xj-xi)*(yj-yk);
      alphaj = (yk-yi)/denom;
      betaj = (xi-xk)/denom;

      denom = (xi-xk)*(yi-yj)-(xi-xj)*(yi-yk);
      alphai = (yk-yj)/denom;
      betai = (xj-xk)/denom;

      denom = (xk-xj)*(yk-yi)-(xk-xi)*(yk-yj);
      alphak = (yj-yi)/denom;
      betak = (xi-xj)/denom;

      // indk
      mat1[m] = _grid->get_tx2(i,j)-1;
      mat2[m] = indk-1;
      mat3[m] = _grid->coef(_grid->get_xinter(i,j),_y[j])*(alphak*fix + betak*fiy);
      //std::cout<<setprecision(19) << mat2[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
      //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      m = m +1;

      // indi
      mat1[m] = _grid->get_tx2(i,j)-1;
      mat2[m] = indi-1;
      mat3[m] = _grid->coef(_grid->get_xinter(i,j),_y[j])*(alphai*fix + betai*fiy);
      //std::cout<<setprecision(19) << mat2[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
      //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      m = m +1;

      // int(i,i+1),j
      mat1[m] = _grid->get_tx2(i,j)-1;
      mat2[m] = _grid->get_tx2(i,j)-1;
      mat3[m] = 1 + _grid->coef(_grid->get_xinter(i,j),_y[j])*(alphaj*fix + betaj*fiy);
      //std::cout<<setprecision(19) << mat2[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
      //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      m = m +1;

      // int(i,i+1),j
      mat1[m] = _grid->get_tx2(i,j)-1;
      mat2[m] = _grid->Get_nbsigne() + _grid->get_zone(_grid->get_tx2(i,j))-1;
      mat3[m] = -1.;
      //std::cout<<setprecision(19) << mat2[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
      //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      m = m +1;


    } else {
      // on fait schema degenere: : derivee ordre 1 dans la direction x

      // ind(i,j)
      mat1[m] =_grid->get_tx2(i,j)-1;
      mat2[m] = _grid->ind(i,j)-1;
      if (fix>=0) {
        mat3[m] =  abs(-_grid->coef(_grid->get_xinter(i,j),_y[j])/(_grid->get_xinter(i,j)-_x[i]));
      }else{
        mat3[m] =  -abs(-_grid->coef(_grid->get_xinter(i,j),_y[j])/(_grid->get_xinter(i,j)-_x[i]));
        //std::cout<<setprecision(19) << mat2[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
        //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      }
      m = m +1;

      // int(i,i+1),j
      mat1[m] = _grid->get_tx2(i,j)-1;
      mat2[m] =  _grid->get_tx2(i,j)-1;
      if (fix>=0) {
        mat3[m] = 1 + abs(_grid->coef(_grid->get_xinter(i,j),_y[j])/(_grid->get_xinter(i,j)-_x[i]));
      }else{
        mat3[m] = 1 - abs(_grid->coef(_grid->get_xinter(i,j),_y[j])/(_grid->get_xinter(i,j)-_x[i]));
      }
      //std::cout<<setprecision(19) << mat2[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
      //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      m = m +1;

      // Um
      mat1[m] = _grid->get_tx2(i,j)-1;
      mat2[m] = _grid->Get_nbsigne() + _grid->get_zone(_grid->get_tx2(i,j))-1;
      mat3[m] = -1.;
      //std::cout<<setprecision(19) << mat2[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
      //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      m = m +1;


    }
  }else {
    // flux � droite
    // je choisis les points suivant la direction de la normale pour calculer la derivee normale
    marqueur = 2;
    xj=0;
    yj=0;
    xk=0;
    yk=0;
    xi=0;
    yi=0;

    xj = _grid->get_xinter(i,j);
    yj = _y[j];

    d = dx-(_grid->get_xinter(i,j) - _x[i]);
    r2 = sqrt(pow((_grid->get_xinter(i,j)-0),2) + pow((_y[j]-0),2));
    the = acos( (_grid->get_xinter(i,j)-0)/r2);
    if (_y[j] <= 0.) the = 2.*pi-the;
    dd = d*tan(the);

    if ((dd >= 0.) && (dd <= dy)) {
      indk = _grid->ind(i+1,j);
      xk = _x[i+1];
      yk = _y[j];
      indi = _grid->ind(i+1,j+1);
      xi = _x[i+1];
      yi = _y[j+1];
      if (_grid->Levelset(i+1,j)*_grid->Levelset(i+1,j+1) <= 0.)  {marqueur = 1;}
    }


    if (dd > dy){
      indk = _grid->ind(i,j+1);
      xk = _x[i];
      yk =  _y[j+1];
      indi = _grid->ind(i+1,j+1);
      xi = _x[i+1];
      yi = _y[j+1];
      if (_grid->Levelset(i+1,j+1)*_grid->Levelset(i,j+1) <= 0.)  {marqueur = 1;}
      if (_grid->Levelset(i+1,j)*_grid->Levelset(i+1,j+1) <= 0.)  {marqueur = 1;}
    }

    if ((dd < 0.) && (dd >= -dy)){
      indk = _grid->ind(i+1,j);
      xk = _x[i+1];
      yk = _y[j];
      indi = _grid->ind(i+1,j-1);
      xi = _x[i+1];
      yi = _y[j-1];
      if (_grid->Levelset(i+1,j)*_grid->Levelset(i+1,j-1) <= 0)  {marqueur = 1;}
    }

    if ((dd < -dy)){
      indk = _grid->ind(i+1,j-1);
      xk = _x[i+1];
      yk = _y[j-1];
      indi = _grid->ind(i,j-1);
      xi = _x[i];
      yi = _y[j-1];
      if (_grid->Levelset(i+1,j)*_grid->Levelset(i+1,j-1) <= 0) {marqueur = 1;}
      if (_grid->Levelset(i+1,j-1)*_grid->Levelset(i,j-1) <= 0)  {marqueur = 1;}
    }

    if (marqueur != 1) {
      // calcul des coef du gradient  (interpolation lineaire sur les 3 points)
      denom = (xj-xk)*(yj-yi)-(xj-xi)*(yj-yk);
      alphaj = (yk-yi)/denom;
      betaj = (xi-xk)/denom;


      denom = (xi-xk)*(yi-yj)-(xi-xj)*(yi-yk);
      alphai = (yk-yj)/denom;
      betai = (xj-xk)/denom;

      denom = (xk-xj)*(yk-yi)-(xk-xi)*(yk-yj);
      alphak = (yj-yi)/denom;
      betak = (xi-xj)/denom;



      // indk
      mat1[m] = _grid->get_tx2(i,j)-1;
      mat2[m] =_grid->get_tx2(i,j)-1;
      mat3[m] = 1 + _grid->coef(_grid->get_xinter(i,j),_y[j])*(alphaj*fix + betaj*fiy);
      //std::cout<<setprecision(19) << mat2[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
      //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      m = m +1;

      // indi
      mat1[m] = _grid->get_tx2(i,j)-1;
      mat2[m] = indk-1;
      mat3[m] = _grid->coef(_grid->get_xinter(i,j),_y[j])*(alphak*fix + betak*fiy);//probleme de signe de betak
      //std::cout<<setprecision(19) << mat2[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
      //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      m = m +1;

      // int(i,i+1),j
      mat1[m] = _grid->get_tx2(i,j)-1;
      mat2[m] = indi-1;
      mat3[m] = _grid->coef(_grid->get_xinter(i,j),_y[j])*(alphai*fix + betai*fiy);
      //std::cout<<setprecision(19) << mat2[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
      //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      m = m +1;

      // Um
      mat1[m] = _grid->get_tx2(i,j)-1;
      mat2[m] = _grid->Get_nbsigne() + _grid->get_zone(_grid->get_tx2(i,j))-1;
      mat3[m] = -1.;
      //std::cout<<setprecision(19) << mat2[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
      //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      m = m +1;


    }else{
      // on fait schema degenere: : derivee ordre 1 dans la direction x

      // ind(i,j)
      mat1[m] =_grid->get_tx2(i,j)-1;
      mat2[m] =_grid->get_tx2(i,j)-1;
      if (fix>=0) {
        mat3[m] = 1 + abs(-_grid->coef(_grid->get_xinter(i,j),_y[j])/(_x[i+1]-_grid->get_xinter(i,j)));
      }else{
        mat3[m] = 1 +  -abs(-_grid->coef(_grid->get_xinter(i,j),_y[j])/(_x[i+1]-_grid->get_xinter(i,j)));
      }
      //std::cout<<setprecision(19) << mat2[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
      //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      m = m +1;

      // int(i,i+1),j
      mat1[m] = _grid->get_tx2(i,j)-1;
      mat2[m] = _grid->ind(i+1,j)-1;
      if (fix>=0) {
        mat3[m] =  abs(_grid->coef(_grid->get_xinter(i,j),_y[j])/(_x[i+1]-_grid->get_xinter(i,j)));
      }else{
        mat3[m] =  -abs(_grid->coef(_grid->get_xinter(i,j),_y[j])/(_x[i+1]-_grid->get_xinter(i,j)));
      }
      //std::cout<<setprecision(19) << mat2[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
      //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      m = m +1;

      // Um
      mat1[m] = _grid->get_tx2(i,j)-1;
      mat2[m] =  _grid->Get_nbsigne() + _grid->get_zone(_grid->get_tx2(i,j))-1;
      mat3[m] = -1.;
      //std::cout<<setprecision(19) << mat2[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
      //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      m = m +1;


    }
  }

}

void HeatAssembler::electrodefluxy(int i, int j){

  pi = acos(-1.);

  fix=0;
  fiy=0;

  //calcul normale sur le point d'interface
  fix = (_grid->get_u(i,j)*(_y[j+1]-_grid->get_yinter(i,j))-_grid->get_u(i,j+1)*(_y[j]-_grid->get_yinter(i,j)))/dy;
  fiy = (_grid->get_v(i,j)*(_y[j+1]-_grid->get_yinter(i,j))-_grid->get_v(i,j+1)*(_y[j]-_grid->get_yinter(i,j)))/dy;
  fiix = fix/sqrt(pow(fix,2)+pow(fiy,2));
  fiiy = fiy/sqrt(pow(fix,2)+pow(fiy,2));
  fix = fiix;
  fiy = fiiy;

  marqueur = 0;
  if (_grid->Levelset(i,j) < 0.) {
    // je calcule le flux � gauche:
    marqueur = 2;

    // je choisis les points suivant la direction de la normale pour calculer la derivee normale
    xj = _x[i];
    yj = _grid->get_yinter(i,j);

    d = dy-(_grid->get_yinter(i,j) - _y[j]);
    r2 = sqrt(pow((_grid->get_yinter(i,j)-0),2) + pow((_x[i]-0),2));
    the = acos( -(_grid->get_yinter(i,j)-0)/r2);
    if (_x[i] <= 0.) the = 2.*pi-the;
    dd = d*tan(the);


    if ((dd < 0.) && (dd >= -dx)) {
      indk = _grid->ind(i,j);
      xk = _x[i];
      yk = _y[j];
      indi = _grid->ind(i-1,j);
      xi = _x[i-1];
      yi = _y[j];
      if (_grid->Levelset(i,j)*_grid->Levelset(i-1,j) <= 0.) {marqueur = 1;}
    }

    if (dd < -dx){
      indk = _grid->ind(i-1,j+1);
      xk = _x[i-1];
      yk =  _y[j+1];
      indi = _grid->ind(i-1,j);
      xi = _x[i-1];
      yi = _y[j];
      if (_grid->Levelset(i,j)*_grid->Levelset(i-1,j) <= 0.) {marqueur = 1;}
      if (_grid->Levelset(i-1,j+1)*_grid->Levelset(i-1,j) <= 0.) {marqueur = 1;}
    }

    if ((dd >= 0.) && (dd <= dx)){
      indk = _grid->ind(i,j);
      xk = _x[i];
      yk = _y[j];
      indi = _grid->ind(i+1,j);
      xi = _x[i+1];
      yi = _y[j];
      if (_grid->Levelset(i,j)*_grid->Levelset(i+1,j)<= 0) {marqueur = 1;}
    }

    if (dd > dx){
      indk = _grid->ind(i+1,j+1);
      xk = _x[i+1];
      yk = _y[j+1];
      indi = _grid->ind(i+1,j);
      xi = _x[i+1];
      yi = _y[j];
      if (_grid->Levelset(i,j)*_grid->Levelset(i+1,j) <= 0) {marqueur = 1;}
      if (_grid->Levelset(i+1,j+1)*_grid->Levelset(i+1,j) <= 0) {marqueur = 1;}
    }

    if (marqueur != 1) {
      // formule ordre 1 non-degeneree, dans la direction de la normale avec les 3 points choisis ci-dessus

      // calcul des coef du gradient (interpolation lineaire sur les 3 points)
      denom = (xj-xk)*(yj-yi)-(xj-xi)*(yj-yk);
      alphaj = (yk-yi)/denom;
      betaj = (xi-xk)/denom;

      denom = (xi-xk)*(yi-yj)-(xi-xj)*(yi-yk);
      alphai = (yk-yj)/denom;
      betai = (xj-xk)/denom;

      denom = (xk-xj)*(yk-yi)-(xk-xi)*(yk-yj);
      alphak = (yj-yi)/denom;
      betak = (xi-xj)/denom;

      // indk
      mat1[m] = _grid->get_ty2(i,j)-1;
      mat2[m] = indk-1;
      mat3[m] = _grid->coef(_x[i],_grid->get_yinter(i,j))*(alphak*fix + betak*fiy);
      //std::cout<<setprecision(19) << mat2[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
      //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      m = m +1;

      // indi
      mat1[m] = _grid->get_ty2(i,j)-1;
      mat2[m] = indi-1;
      mat3[m] = _grid->coef(_x[i],_grid->get_yinter(i,j))*(alphai*fix + betai*fiy);
      //std::cout<<setprecision(19) << mat2[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
      //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      m = m +1;

      // int(i,i+1),j
      mat1[m] = _grid->get_ty2(i,j)-1;
      mat2[m] = _grid->get_ty2(i,j)-1;
      mat3[m] = 1 + _grid->coef(_x[i],_grid->get_yinter(i,j))*(alphaj*fix + betaj*fiy);
      //std::cout<<setprecision(19) << mat2[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
      //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      m = m +1;

      // Um
      mat1[m] = _grid->get_ty2(i,j)-1;
      mat2[m] = _grid->Get_nbsigne() + _grid->get_zone(_grid->get_ty2(i,j))-1;
      mat3[m] = -1.;
      //std::cout<<setprecision(19) << mat2[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
      //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      m = m +1;


    } else {
      // on fait schema degenere: : derivee ordre 1 dans la direction x

      // ind(i,j)
      mat1[m] =_grid->get_ty2(i,j)-1;
      mat2[m] = _grid->ind(i,j)-1;
      if (fiy>=0) {
        mat3[m] =  abs(-_grid->coef(_x[i],_grid->get_yinter(i,j))/(_grid->get_yinter(i,j)-_y[j]));
      }else{
        mat3[m] =  -abs(-_grid->coef(_x[i],_grid->get_yinter(i,j))/(_grid->get_yinter(i,j)-_y[j]));
      }
      //std::cout<<setprecision(19) << mat2[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
      //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      m = m +1;

      // int(i,i+1),j
      mat1[m] = _grid->get_ty2(i,j)-1;
      mat2[m] =  _grid->get_ty2(i,j)-1;
      if (fiy>=0) {
        mat3[m] = 1 + abs(-_grid->coef(_x[i],_grid->get_yinter(i,j))/(_grid->get_yinter(i,j)-_y[j]));
      }else{
        mat3[m] = 1 -abs(-_grid->coef(_x[i],_grid->get_yinter(i,j))/(_grid->get_yinter(i,j)-_y[j]));
      }
      //std::cout<<setprecision(19) << mat2[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
      //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      m = m +1;

      // Um
      mat1[m] = _grid->get_ty2(i,j)-1;
      mat2[m] = n + _grid->get_zone(_grid->get_ty2(i,j))-1;
      mat3[m] = -1.;
      //std::cout<<setprecision(19) << mat2[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
      //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      m = m +1;


    }
  }else {
    // flux � droite
    // je choisis les points suivant la direction de la normale pour calculer la derivee normale
    marqueur = 2;
    xj=0;
    yj=0;
    xk=0;
    yk=0;
    xi=0;
    yi=0;

    xj = _x[i];
    yj = _grid->get_yinter(i,j);


    d = (_grid->get_yinter(i,j) - _y[j]);
    r2 = sqrt(pow((_grid->get_yinter(i,j)-0),2) + pow((_x[i]-0),2));
    the = acos( -(_grid->get_yinter(i,j)-0)/r2);
    if (_x[i] <= 0.) the = 2.*pi-the;
    dd = -d*tan(the);

    if ((dd >= 0.) && (dd <= dx)) {
      indk = _grid->ind(i,j+1);
      xk = _x[i];
      yk = _y[j+1];
      indi = _grid->ind(i+1,j+1);
      xi = _x[i+1];
      yi = _y[j+1];
      if (_grid->Levelset(i,j+1)*_grid->Levelset(i+1,j+1) <= 0.)  {marqueur = 1;}
    }


    if (dd > dx){
      indk = _grid->ind(i+1,j);
      xk = _x[i+1];
      yk =  _y[j];
      indi = _grid->ind(i+1,j+1);
      xi = _x[i+1];
      yi = _y[j+1];
      if (_grid->Levelset(i+1,j+1)*_grid->Levelset(i+1,j) <= 0.)  {marqueur = 1;}
      if (_grid->Levelset(i,j+1)*_grid->Levelset(i+1,j+1) <= 0.)  {marqueur = 1;}
    }

    if ((dd < 0.) && (dd >= -dx)){
      indk = _grid->ind(i,j+1);
      xk = _x[i];
      yk = _y[j+1];
      indi = _grid->ind(i-1,j+1);
      xi = _x[i-1];
      yi = _y[j+1];
      if (_grid->Levelset(i,j+1)*_grid->Levelset(i-1,j+1) <= 0)  {marqueur = 1;}
    }

    if ((dd < -dx)){
      indk = _grid->ind(i-1,j+1);
      xk = _x[i-1];
      yk = _y[j+1];
      indi = _grid->ind(i-1,j);
      xi = _x[i-1];
      yi = _y[j];
      if (_grid->Levelset(i,j+1)*_grid->Levelset(i-1,j+1) <= 0) {marqueur = 1;}
      if (_grid->Levelset(i-1,j+1)*_grid->Levelset(i-1,j) <= 0)  {marqueur = 1;}
    }

    if (marqueur != 1) {
      // calcul des coef du gradient  (interpolation lineaire sur les 3 points)
      denom = (xj-xk)*(yj-yi)-(xj-xi)*(yj-yk);
      alphaj = (yk-yi)/denom;
      betaj = (xi-xk)/denom;


      denom = (xi-xk)*(yi-yj)-(xi-xj)*(yi-yk);
      alphai = (yk-yj)/denom;
      betai = (xj-xk)/denom;

      denom = (xk-xj)*(yk-yi)-(xk-xi)*(yk-yj);
      alphak = (yj-yi)/denom;
      betak = (xi-xj)/denom;
      //cout<<setprecision(20)<<denom<<";"<<xi<<";"<<xj<<endl;


      // indk
      mat1[m] = _grid->get_ty2(i,j)-1;
      mat2[m] =_grid->get_ty2(i,j)-1;
      mat3[m] = 1 + _grid->coef(_x[i],_grid->get_yinter(i,j))*(alphaj*fix + betaj*fiy);
      //std::cout<<setprecision(19) << mat2[m]<<";"<<i<<";"<<j<<";"<<m<<";"<<std::endl;
      //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      m = m +1;

      // indi
      mat1[m] = _grid->get_ty2(i,j)-1;
      mat2[m] = indk-1;
      mat3[m] = _grid->coef(_x[i],_grid->get_yinter(i,j))*(alphak*fix + betak*fiy);//probleme de signe de betak
      //std::cout<<setprecision(19) << mat2[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
      //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      m = m +1;

      // int(i,i+1),j
      mat1[m] = _grid->get_ty2(i,j)-1;
      mat2[m] = indi-1;
      mat3[m] = _grid->coef(_x[i],_grid->get_yinter(i,j))*(alphai*fix + betai*fiy);
      //std::cout<<setprecision(19) << mat2[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
      //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      m = m +1;

      // Um
      mat1[m] = _grid->get_ty2(i,j)-1;
      mat2[m] = _grid->Get_nbsigne() + _grid->get_zone(_grid->get_ty2(i,j))-1;
      mat3[m] = -1.;
      //std::cout<<setprecision(19) << mat2[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
      //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      m = m +1;


    }else{
      // on fait schema degenere: : derivee ordre 1 dans la direction x

      // ind(i,j)
      mat1[m] =_grid->get_ty2(i,j)-1;
      mat2[m] =_grid->get_ty2(i,j)-1;
      if (fiy>=0) {
        mat3[m] = 1 + abs(-_grid->coef(_x[i],_grid->get_yinter(i,j))/(_y[j+1]-_grid->get_yinter(i,j)));
      }else{
        mat3[m] = 1 - abs(-_grid->coef(_x[i],_grid->get_yinter(i,j))/(_y[j+1]-_grid->get_yinter(i,j)));
      }
      //std::cout<<setprecision(19) << mat2[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
      //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      m = m +1;

      // int(i,i+1),j
      mat1[m] = _grid->get_ty2(i,j)-1;
      mat2[m] = _grid->ind(i+1,j)-1;
      if (fiy>=0) {
        mat3[m] =  abs(_grid->coef(_x[i],_grid->get_yinter(i,j))/(_y[j+1]-_grid->get_yinter(i,j)));
      }else{
        mat3[m] =  -abs(_grid->coef(_x[i],_grid->get_yinter(i,j))/(_y[j+1]-_grid->get_yinter(i,j)));
      }
      //std::cout<<setprecision(19) << mat2[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
      //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      m = m +1;

      // Um
      mat1[m] = _grid->get_ty2(i,j)-1;
      mat2[m] = n + _grid->get_zone(_grid->get_ty2(i,j))-1;
      mat3[m] = -1.;
      //std::cout<<setprecision(19) << mat2[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
      //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      m = m +1;


    }
  }

}


void HeatAssembler::BuildLaplacianMatrix()
{

  _grid->BuildT();
  _grid->inter();
  _grid->DLevelset();
  _grid->Zonelectrode();


  _lap_mat.resize(Nx*Ny+_grid->Get_nbsigne()+Ne,Nx*Ny+_grid->Get_nbsigne()+Ne);

  coeff = 0;
  m=0;

  for (int j=0; j<Ny; ++j){
    for (int i=0; i<Nx; ++i){

      mat1[m]= _grid->ind(i,j)-1;
      mat2[m]= _grid->ind(i,j)-1;
      mat3[m]=0;

      coeff = _grid->coef(_x[i]+dx/2,_y[j]);
      //cout<< _grid->coef(_x[i]+dx/2,_y[j])<<_x[i]+dx/2<<<<endl;
      if (_grid->get_xix2_2(i,j) != 0)  coeff = _grid->coef(_grid->get_xinter(i,j)/2 + _x[i]/2, _y[j]);
      mat3[m] =  coeff*( -1./(dx*(1. - _grid->get_xix2_2(i,j)) +_grid->get_xix2_2(i,j)*abs(_grid->get_xinter(i,j)-_x[i])))/dx;

      coeff = _grid->coef(_x[i]-dx/2,_y[j]);
      if(i==0) {
        mat3[m] = mat3[m] + coeff*( -1./(dx*(1.)))/dx;
      }else{
        if (_grid->get_xix2_2(i-1,j) != 0)  coeff = _grid->coef(_grid->get_xinter(i-1,j)/2 + _x[i]/2, _y[j]);
        mat3[m] = mat3[m] + coeff*( -1./(dx*(1. -_grid->get_xix2_2(i-1,j)) +_grid->get_xix2_2(i-1,j)*abs(_x[i]- _grid->get_xinter(i-1,j))))/dx;
      }

      coeff = _grid->coef(_x[i],_y[j] + dy/2.);
      if (_grid->get_xiy2_2(i,j) != 0)  coeff = _grid->coef(_x[i], _grid->get_yinter(i,j)/2 + _y[j]/2);
      mat3[m] = mat3[m] + coeff*( -1./(dy*(1. - _grid->get_xiy2_2(i,j)) +_grid->get_xiy2_2(i,j)*abs(_grid->get_yinter(i,j)-_y[j])))/dx;

      coeff = _grid->coef(_x[i],_y[j]-dy/2);
      if (j==0){
        mat3[m] = mat3[m] + coeff*( -1./(dy*(1.)))/dx;
      }else{
        if (_grid->get_xiy2_2(i,j-1) != 0)  coeff = _grid->coef(_x[i], _grid->get_yinter(i,j-1)/2. + _y[j]/2.);
        mat3[m] = mat3[m] + coeff*( -1./(dy*(1. -_grid->get_xiy2_2(i,j-1)) +_grid->get_xiy2_2(i,j-1)*abs(_y[j]- _grid->get_yinter(i,j-1))))/dx;
      }

      //std::cout<<setprecision(17) <<coeff<<";"<<mat3[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
      //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
      m=m+1;

      if (i-1 >= 0){
        coeff = _grid->coef(_x[i]-dx/2.,_y[j]);
        if (_grid->get_xix2_2(i-1,j) != 0)  coeff = _grid->coef(_grid->get_xinter(i-1,j)/2. + _x[i]/2., _y[j]);
        mat1[m] = _grid->ind(i,j)-1;
        mat2[m] = (1-_grid->get_xix2_2(i-1,j))*_grid->ind(i-1,j) + _grid->get_xix2_2(i-1,j)*_grid->get_tx2(i-1,j)-1;
        mat3[m] =  coeff*((1-_grid->get_xix2_2(i-1,j))/(dx*dx)  + _grid->get_xix2_2(i-1,j)/(abs(_x[i]-_grid->get_xinter(i-1,j))*dx));
        //std::cout<<setprecision(19) <<  mat1[m]<<";"<<  mat2[m]<<";"<<  mat3[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
        //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
        m = m +1;
      }
      // i+1,j
      if (i+1 <= Nx-1){
        coeff = _grid->coef(_x[i]+dx/2.,_y[j]);
        if (_grid->get_xix2_2(i,j) != 0)  coeff = _grid->coef(_grid->get_xinter(i,j)/2. + _x[i]/2, _y[j]);
        mat1[m] = _grid->ind(i,j)-1;
        mat2[m] = ((1-  _grid->get_xix2_2(i,j))*_grid->ind(i+1,j) + _grid->get_xix2_2(i,j)*_grid->get_tx2(i,j))-1;
        mat3[m] = coeff*((1-_grid->get_xix2_2(i,j))/(dx*dx) + _grid->get_xix2_2(i,j)/(abs( _grid->get_xinter(i,j)-_x[i])*dx));
        //std::cout<<setprecision(19) <<  mat1[m]<<";"<<  mat2[m]<<";"<<  mat3[m]<<";"<<i<<";"<<j<<";"<<m<<"hi"<<std::endl;
        //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
        m = m +1;
      }
      //i,j-1
      if (j-1 >= 0){
        coeff = _grid->coef(_x[i],_y[j]-dy/2.);
        if (_grid->get_xiy2_2(i,j-1) != 0)  coeff = _grid->coef(_x[i], _grid->get_yinter(i,j-1)/2. + _y[j]/2.);
        mat1[m] = _grid->ind(i,j)-1;
        mat2[m] =  (1-_grid->get_xiy2_2(i,j-1))*_grid->ind(i,j-1) + _grid->get_xiy2_2(i,j-1)*_grid->get_ty2(i,j-1)-1;
        mat3[m] = coeff*((1-_grid->get_xiy2_2(i,j-1))/(dy*dy)  + _grid->get_xiy2_2(i,j-1)/(abs(_y[j]-_grid->get_yinter(i,j-1))*dy));
        //std::cout<<setprecision(19) <<  mat1[m]<<";"<<  mat2[m]<<";"<<  mat3[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
        //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
        m = m +1;
      }
      //i,j+1
      if (j+1 <= Nx-1){
        coeff = _grid->coef(_x[i],_y[j] +dy/2.);
        if (_grid->get_xiy2_2(i,j) != 0)  coeff = _grid->coef(_x[i], _grid->get_yinter(i,j)/2. + _y[j]/2.);
        mat1[m] = _grid->ind(i,j)-1;
        mat2[m] =  ((1-  _grid->get_xiy2_2(i,j))*_grid->ind(i,j+1) + _grid->get_xiy2_2(i,j)*_grid->get_ty2(i,j))-1;
        mat3[m] = coeff*((1-_grid->get_xiy2_2(i,j))/(dy*dy) + _grid->get_xiy2_2(i,j)/(abs( _grid->get_yinter(i,j)-_y[j])*dy));
        //std::cout<<setprecision(19) <<  mat1[m]<<";"<<  mat2[m]<<";"<<  mat3[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
        //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
        m = m +1;
      }
//cout<< _grid->coef(_x[i],_y[j])<<";"<<_x[i]<<";"<<_y[j]<<";"<<i<<";"<<j<<endl;
    }
  }




  for (int j=1; j< Ny-1; ++j){
    for (int i=1; i<Nx-1; ++i){

      // s'il y a un point d'interface entre i et i+1
      if ((_grid->get_tx2(i,j))!= 0) {
        if (_grid->get_zone(_grid->get_tx2(i,j)) == 0) {
          fluxx(i,j);
        }else{
          electrodefluxx(i,j);
        }
      }

      // s'il y a un point d'interface entre i et i+1
      if ((_grid->get_ty2(i,j))!= 0) {
        if (_grid->get_zone(_grid->get_ty2(i,j)) == 0) {
          fluxy(i,j);
        }else{
          electrodefluxy(i,j);
        }
      }


    }
  }



  r_0 = 1;
  // longueur arc de cercle electrode
  pi = acos(-1.);
  //dthetaa =pi/(2*Ne);
  //Long = r_0*2.*dthetaa;
  //Um = 0.
  somme = 0;

  //for(int i=0; i<Ne; ++i)
  //{
    //valeur à imposer pour courant Im
    //Im(i) = exp(pow(r_0,2))*2.*r_0*Long;
  //}

  for(int i=0; i<_grid->getNbPoints(2); i++){
    Um(i) = 0;
  }


  for(int i=1; i<Nx-1; i++){
    for(int j=1; j<Ny-1; j++){

      if (_grid->get_tx2(i,j) != 0) {
        if (_grid->get_zone(_grid->get_tx2(i,j))  != 0) {
          //std::cout<<setprecision(19) << Um(_grid->get_zone(_grid->get_tx2(i,j))-1)<<std::endl;
          Um(_grid->get_zone(_grid->get_tx2(i,j))-1) =  Um(_grid->get_zone(_grid->get_tx2(i,j))-1) + dx*dy*_grid->Delta(i,j);
          mat1(m) = _grid->Get_nbsigne() + _grid->get_zone(_grid->get_tx2(i,j))-1;
          mat2(m) =  _grid->ind(i,j)-1;
          mat3(m) = -dx*dy*_grid->Delta(i,j);
          //std::cout<<setprecision(19) <<  mat1[m]<<";"<<Um(_grid->get_zone(_grid->get_tx2(i,j))-1)<<std::endl;
          //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
          m = m +1;
        }
      }else{
        if (_grid->get_tx2(i-1,j) != 0) {
          if (_grid->get_zone(_grid->get_tx2(i-1,j))  != 0) {
            Um(_grid->get_zone(_grid->get_tx2(i-1,j))-1) =  Um(_grid->get_zone(_grid->get_tx2(i-1,j))-1) + dx*dy*_grid->Delta(i,j);
            mat1(m) = _grid->Get_nbsigne() + _grid->get_zone(_grid->get_tx2(i-1,j))-1;
            mat2(m) =  _grid->ind(i,j)-1;
            mat3(m) = -dx*dy*_grid->Delta(i,j);
            //  std::cout<<setprecision(19) <<  mat1[m]<<";"<<  mat2[m]<<";"<<  mat3[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
            //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
            m = m +1;
          }
        }else{
          if (_grid->get_ty2(i,j) != 0) {
            if (_grid->get_zone(_grid->get_ty2(i,j))  != 0) {
              //cout<< _grid->Get_zone()(_grid->Get_Ty2()(3,2))<<"helooo"<<3<<2<<endl;
              Um(_grid->get_zone(_grid->get_ty2(i,j))-1) =  Um(_grid->get_zone(_grid->get_ty2(i,j))-1) + dx*dy*_grid->Delta(i,j);
              mat1(m) = _grid->Get_nbsigne() + _grid->get_zone(_grid->get_ty2(i,j))-1;
              mat2(m) =  _grid->ind(i,j)-1;
              mat3(m) = -dx*dy*_grid->Delta(i,j);
              //std::cout<<setprecision(19) <<  mat1[m]<<";"<<  mat2[m]<<";"<<  mat3[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
              //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
              m = m +1;
            }
          }else{
            if (_grid->get_ty2(i,j-1) != 0) {
              if (_grid->get_zone(_grid->get_ty2(i,j-1))  != 0) {
                Um(_grid->get_zone(_grid->get_ty2(i,j-1))-1) =  Um(_grid->get_zone(_grid->get_ty2(i,j-1))-1) + dx*dy*_grid->Delta(i,j);
                mat1(m) = _grid->Get_nbsigne() + _grid->get_zone(_grid->get_ty2(i,j-1))-1;
                mat2(m) =  _grid->ind(i,j)-1;
                mat3(m) = -dx*dy*_grid->Delta(i,j);
                // std::cout<<setprecision(19) <<  mat1[m]<<";"<<  mat2[m]<<";"<<  mat3[m]<<";"<<i<<";"<<j<<";"<<m<<std::endl;
                //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
                m = m +1;
              }
            }
          }
        }
      }

    }
  }

  for(int i=0; i<Ne; i++){
    mat1(m) = _grid->Get_nbsigne() + i;
    mat2(m) = _grid->Get_nbsigne() + i;
    mat3[m] = Um(i);
     //std::cout<<setprecision(19) <<  mat1[m]<<";"<<  mat2[m]<<";"<<  mat3[m]<<";"<<m<<std::endl;
    if (i==0) {mat3[m] = mat3[m] + 0.0000000001;}
    //trp.push_back(Trip(mat1[m],mat2[m],mat3[m]));
    m = m +1;
  }

  m = m-1;

  //vector<Triplet<double>> triplets(m+1);

  matt1.resize(m+1);
  matt2.resize(m+1);
  matt3.resize(m+1);

  for(int i=0; i< m+1; i++){
    matt1[i]=mat1[i];
    matt2[i]=mat2[i];
    matt3[i]=mat3[i];
    trp.push_back(Trip(matt1[i],matt2[i],matt3[i]));
  }

  _lap_mat.setFromTriplets(trp.begin(), trp.end());


}

void HeatAssembler::CLEAR()
{
trp.clear();
matt1.resize(0);
matt2.resize(0);
matt3.resize(0);
}

void HeatAssembler::BuildSourceTerm()
{
  _source_term.resize(Nx*Ny+_grid->Get_nbsigne()+Ne);

  for(int i=0; i<Nx*Ny+_grid->Get_nbsigne()+Ne; ++i)
  {
    _source_term[i]=0;
  }

  for(int i=0; i<Nx; ++i)
  {
    for(int j=0; j<Ny; ++j)
    {
      _source_term[_grid->ind(i,j)-1] == 0;

      if (_grid->get_tx2(i,j) != 0) {
       _source_term[_grid->get_tx2(i,j)-1]=0;
       //cout<<_source_term[_grid->Get_Tx2()(i,j)]<<";"<<_grid->Get_Tx2()(i,j)-1<<endl;
      }
      if (_grid->get_ty2(i,j) != 0) {
       _source_term[_grid->get_ty2(i,j)-1]=0;
       //cout<<_source_term[_grid->Get_Ty2()(i,j)-1]<<";"<<_grid->Get_Ty2()(i,j)-1<<endl;
      }

    }
  }

  for(int i=0; i<Ne; ++i)
  {
    _source_term[_grid->Get_nbsigne() +i+1-1]=Im(i);
  }

}

void HeatAssembler::BuildSourceTerm2()
{
  _source_term2.resize(Nx*Ny+_grid->Get_nbsigne()+Ne);

  for(int j=0; j<Ny; ++j)
  {
    for(int i=0; i<Nx; ++i)
    {
      _source_term2[_grid->ind(i,j)-1] = exp( pow(_x[i],2) + pow(_y[j],2))*(4. + 4.*pow(_x[i],2) + 4.*pow(_y[j],2));
      //cout<< _source_term2[_grid->ind(i,j)-1]<<";"<<_grid->ind(i,j)-1<<endl;
    }
  }

  for(int j=0; j<Ny; ++j)
  {
    for(int i=0; i<Nx; ++i)
    {
      if (_grid->get_tx2(i,j) != 0) {
        //cout<< _grid->get_tx2(i,j)-1<<";"<<i<<";"<<j<<endl;
        if (_grid->get_zone(_grid->get_tx2(i,j))  == 0) {
          //cout<< _grid->get_tx2(i,j)-1<<";"<<i<<";"<<j<<endl;
          fix = (_grid->get_u(i,j)*(_x[i+1]-_grid->get_xinter(i,j))-_grid->get_u(i+1,j)*(_x[i]-_grid->get_xinter(i,j)))/dx;
          fiy = (_grid->get_v(i,j)*(_x[i+1]-_grid->get_xinter(i,j))-_grid->get_v(i+1,j)*(_x[i]-_grid->get_xinter(i,j)))/dx;
          fiix = fix/sqrt(pow(fix,2)+pow(fiy,2));
          fiiy = fiy/sqrt(pow(fix,2)+pow(fiy,2));
          fix = fiix;
          fiy = fiiy;
          _source_term2[_grid->get_tx2(i,j)-1] =_grid->coef(_grid->get_xinter(i,j),_y[j])*(2.*_grid->get_xinter(i,j)*fix + 2.*_y[j]*fiy)*exp(pow(_grid->get_xinter(i,j),2)+ pow(_y[j],2));
          //cout<< _source_term2[_grid->get_tx2(i,j)-1]<<";"<<_grid->get_tx2(i,j)-1<<endl;
        }
        else{
          _source_term2[_grid->get_tx2(i,j)-1] = 0 ;
          //cout<< _source_term2[_grid->get_tx2(i,j)-1]<<";"<<_grid->get_tx2(i,j)-1<<endl;
        }
      }
      if (_grid->get_ty2(i,j) != 0) {
        if (_grid->get_zone(_grid->get_ty2(i,j))  == 0) {
          fix = (_grid->get_u(i,j)*(_y[j+1]-_grid->get_yinter(i,j))-_grid->get_u(i,j+1)*(_y[j]-_grid->get_yinter(i,j)))/dy;
          fiy = (_grid->get_v(i,j)*(_y[j+1]-_grid->get_yinter(i,j))-_grid->get_v(i,j+1)*(_y[j]-_grid->get_yinter(i,j)))/dy;
          fiix = fix/sqrt(pow(fix,2)+pow(fiy,2));
          fiiy = fiy/sqrt(pow(fix,2)+pow(fiy,2));
          fix = fiix;
          fiy = fiiy;
          _source_term2[_grid->get_ty2(i,j)-1] =  _grid->coef(_x[i],_grid->get_yinter(i,j))*(2.*_x[i]*fix + 2.*_grid->get_yinter(i,j)*fiy)*exp(pow(_x[i],2)+pow(_grid->get_yinter(i,j),2));
          //cout<< _source_term2[_grid->get_ty2(i,j)-1]<<";"<<_grid->get_ty2(i,j)-1<<endl;
        }
        else{
          _source_term2[_grid->get_ty2(i,j)-1] = 0 ;
          //cout<< _source_term2[_grid->get_ty2(i,j)-1]<<";"<<_grid->get_ty2(i,j)-1<<endl;

        }
      }

    }
  }


  pi= acos(-1.);
  for(int i=0; i<Ne; i++){
    //valeur à imposer pour courant Im
    Im(i) = exp(pow(0.5,2))*2.*0.5*pi/(2*Ne);
  }

  for(int i=0; i<Ne; ++i)
  {
    _source_term2[_grid->Get_nbsigne() +i+1-1]=  Im(i) ;
  }



}
