#ifndef _HEAT_PROBLEM_HPP_
#define _HEAT_PROBLEM_HPP_


#include "parameters.hpp"
#include "grid.hpp"
#include "assembler.hpp"
#include "writer.hpp"
//#include <IterativeLinearSolvers>
#include "Eigen/Eigen/IterativeLinearSolvers"
//#include <../unsupported/Eigen/IterativeSolvers>
#include "Eigen/unsupported/Eigen/src/IterativeSolvers/GMRES.h"



// A class that solve the heat equation on a cartesian grid
// ...
// ... details
// ...
class HeatProblem {

  public:

    // Initializes the problem from a text input file
    HeatProblem(Parameters& p);

    // Does the whole computation of the solution
    void run();

    void runwithsource();

    // source
    double sourceTerm();

    // Diffusion coefficient is scalar in this case
    double getDiffusionCoeff() {return _k;};

    // Récupère la Solution
    Eigen::VectorXd  GetSolution() {return _sol;};




  private:

    // Initialization method
    //void startUp();

    Parameters*   _params;
    Grid          _grid;
    HeatAssembler _assbl;
    Writer        _writer;
    Eigen::VectorXd _x, _y;

    double _k;
    // Solution
    Eigen::VectorXd _sol,residu,yy,xx,xxx;
    Eigen::VectorXd sigma;
    // Solver
    SparseLU<SparseMatrix<double>, COLAMDOrdering<int> > _solver;
    Eigen::GMRES<SparseMatrix<double>,Eigen::IncompleteLUT< double, int>> gmres;
    Eigen::BiCGSTAB<SparseMatrix<double>,Eigen::IncompleteLUT< double, int>> bicg;
    Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> _dxsol,_dysol;


    double exactsol,linf,errmax,resmax,xp,xm,uup,uum,errdxmax,dxsolexact;


};

#endif//_HEAT_PROBLEM_HPP_
