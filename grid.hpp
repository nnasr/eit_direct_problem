#ifndef _GRID_HPP_
#define _GRID_HPP_

#include "Eigen/Eigen/Dense"
#include "Eigen/Eigen/Sparse"
#include "parameters.hpp"

// A cartesian grid. The size of the domain is fixed: it is [0,1]x[0,1]
class Grid {

  public:

    Grid() {};
    // Constructor with configuration (reads number of points in each direction)
    Grid(Parameters& p);

    // Returns the spacing discretization step in direction dim (0:x, 1:y)
    long double getSpacing(uint dim);

    // Returns the number of points in given dimension
    uint getNbPoints(uint dim);

    // Gives the matrix row ID given position in grid
    //  uint index1D(uint i, uint j);

    // Gives the position in grid from matrix row index
    void index2D(uint row,uint& i, uint& j);

    // Get coordinates
    void coords(uint i, uint j, double&x, double& y);

    // Get coordinates
    long double Levelset( uint i,  uint j) ;

    // Construit le vecteur de numerotation des point irregulier
    void BuildT();

    // Gives the matrix row ID given position in grid  in irregular counting
    uint ind(uint i, uint j);

    //Dertrmnination du numéro de l'electrode auquel appartient le point (x_i,y_j)
    double Funczone( double thetha);

    //Construit la derivé de la level set
    void DLevelset();

	  //Construit la fonction Delta pour discretisé l'integrale
	  double Delta(int i, int j);

    //retrouve les point d'interface
    void inter();

    //retrouve les point d'interface
    void Zonelectrode();

    //conductivity function
    double coef(double x, double y);


    // Pour récuperer la matrice du laplacien
    int get_tx2(int i, int j){return _tx2(i,j);};
    int get_ty2(int i, int j){return _ty2(i,j);};
    int get_xix2_2(int i, int j){return _xix2_2(i,j);};
    int get_xiy2_2(int i, int j){return _xiy2_2(i,j);};
    double get_xinter(int i, int j){return xinter(i,j);};
    double get_yinter(int i, int j){return yinter(i,j);};
    double get_u(int i, int j){return _u(i,j);};
    double get_v(int i, int j){return _v(i,j);};
    //double get_delta(int i, int j){return _delta(i,j);};
    Eigen::VectorXd Get_zone() {return zone;};
    int get_zone(int i) {return zone(i);};
    double Get_nbsigne() {return _nbsigne;};


  private:

    std::array<uint  ,3> _N;
    std::array<double,2> _h;
    int _r,_nbsigne;
    double _dxphiplus,_dxphimoins,_dxphizero;
    double _dyphiplus,_dyphimoins,_dyphizero;
    double _absgrad;
    double _delta;



    Eigen::Matrix<int, Eigen::Dynamic, Eigen::Dynamic> _ty2,_tx2;
    Eigen::Matrix<int, Eigen::Dynamic, Eigen::Dynamic> _xiy2_2,_xix2_2;
    Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> _u,_v;
  	Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> xinter,yinter;
    //Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> _dxphiplus,_dxphimoins,_dxphizero;
    //Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> _dyphiplus,_dyphimoins,_dyphizero;
    //Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic>  _absgrad,_delta;
    Eigen::VectorXd _x, _y;
    Eigen::VectorXd zone;


};




#endif//_GRID_HPP_
