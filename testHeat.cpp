#include "heatProblem.hpp"
#include "parameters.hpp"
#include <iostream>


using namespace std;
using namespace Eigen;



int main(int argc,char* argv[]) {

  Parameters p("params.in");

  // Check saved configuration
  p.display();
  HeatProblem pb(p);

  pb.runwithsource();
  pb.runwithsource();
  return 0;
}
