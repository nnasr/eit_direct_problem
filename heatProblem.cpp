#include "heatProblem.hpp"
#include <cmath>
#include <iostream>
#include <stdio.h>
#include <time.h>
#include <cstdlib>
#include <iomanip>
#include <fstream>
#include "Eigen/Eigen/Eigen"
#include "Eigen/Eigen/Dense"
#include "Eigen/Eigen/Core"
#include "Eigen/Eigen/Sparse"
#include "Eigen/Eigen/IterativeLinearSolvers"
//#include "Eigen/unsupported/Eigen/IterativeSolvers"
#include "Eigen/unsupported/Eigen/src/IterativeSolvers/GMRES.h"


HeatProblem::HeatProblem(Parameters& p) {

  _params = &p;
  _k  = p.getReal("diffusion coefficient",1.);
  _grid   = Grid(p);
  _assbl  = HeatAssembler(&_grid,this);
  _writer = Writer(p,_grid);

  _x.resize(_grid.getNbPoints(0)); // (xmin+h, ..., xmax-h)
  for (int i = 0 ; i < _grid.getNbPoints(0) ; ++i)
  _x[i] =  ((i+1)-0.5)*_grid.getSpacing(0) -1.;

  _y.resize(_grid.getNbPoints(1));
  for (int i = 0 ; i < _grid.getNbPoints(1) ; ++i)
  _y[i] =  ((i+1)-0.5)*_grid.getSpacing(1) -1.;

}

void HeatProblem::run() {
   //Immm.resize(_grid.getNbPoints(2));
  _assbl.BuildLaplacianMatrix();
  _assbl.BuildSourceTerm();

  _sol.resize(_grid.getNbPoints(0)*_grid.getNbPoints(1) + _grid.Get_nbsigne() + _grid.getNbPoints(2));
  for(int i=0;i<_grid.getNbPoints(0)*_grid.getNbPoints(1) + _grid.Get_nbsigne() + _grid.getNbPoints(2);i++){
     _sol[i]=0;
  }



_solver.compute(_assbl.GetMatrix());

if(_solver.info()==Success) {
  std::cout<<""<<std::endl;
  std::cout<<"The decomposition succeded"<<std::endl;
}
_sol = _solver.solve(_assbl. GetSourceTerm());
if(_solver.info()==Success) {
  std::cout<<"Solving the system suceeded, we have a solution of the EIT problem!!"<<std::endl;
}

   double  sumu=0 ;
    for(int i=0; i<_grid.getNbPoints(2); i++){
     sumu = sumu + _sol[_grid.Get_nbsigne()+i];
   }

  for(int i=0;i<_grid.getNbPoints(0)*_grid.getNbPoints(1) + _grid.Get_nbsigne() + _grid.getNbPoints(2);i++){
    _sol[i]=_sol[i]-sumu/_grid.getNbPoints(2);
  }
  //_writer.write(_sol);

  //for(int i=0;i<_grid.getNbPoints(2);i++){
  //  std::cout<<std::setprecision(17)<<Immm(i)<<";"<<_sol[_grid.Get_nbsigne() + i]<<std::endl;
  //}

  //std::fstream my_file;
	//my_file.open("test_8.txt", std::ios::out);
	//if (!my_file) {
	//	std::cout << "File not created!";
	//}
	//else {
  //  for(int i=0;i<_grid.getNbPoints(2);i++){
  //    my_file <<std::setprecision(17)<<Immm(i)<<"      "<<_sol[_grid.Get_nbsigne() + i]<<std::endl;
  //  }
	//	my_file.close();
	//}
  _assbl.CLEAR();

}


////////////////////////////////////////////////////////////////////////////////

void HeatProblem::runwithsource(){

  float temps;
  clock_t t1, t2;

  _grid.BuildT();
  _grid.DLevelset();
  _grid.inter();
  _assbl.BuildLaplacianMatrix();
  _assbl.BuildSourceTerm2();


  _sol.resize(_grid.getNbPoints(0)*_grid.getNbPoints(1) + _grid.Get_nbsigne() + _grid.getNbPoints(2));
  yy.resize(_grid.getNbPoints(0)*_grid.getNbPoints(1) + _grid.Get_nbsigne() + _grid.getNbPoints(2));
  xx.resize(_grid.getNbPoints(0)*_grid.getNbPoints(1) + _grid.Get_nbsigne() + _grid.getNbPoints(2));
  residu.resize(_grid.getNbPoints(0)*_grid.getNbPoints(1) + _grid.Get_nbsigne() + _grid.getNbPoints(2));

  for(unsigned short j=0;j<((unsigned short)(_grid.getNbPoints(1)));j++){
    for(unsigned short i=0;i<((unsigned short)(_grid.getNbPoints(0)));i++){
      _sol[_grid.ind(i,j)-1]=0;
    }
  }

  ////////////////////////////////////////////
  float time = 0;
  t1 = clock();
  //(_assbl.GetMatrix()).makeCompressed();
  //_solver.analyzePattern(_assbl.GetMatrix());
  _solver.compute(_assbl.GetMatrix());
  if(_solver.info()!=Success) {
    std::cout<<"the decomposition of the matrix failed"<<std::endl;
    return;
  }
  if(_solver.info()==Success) {
    std::cout<<"the decomposition succeded"<<std::endl;
  }
  _sol = _solver.solve(_assbl.GetSourceTerm2());
  if(_solver.info()==Success) {
    std::cout<<"solving the system succeded"<<std::endl;
  }

  t2 = clock();
  temps = (float)(t2-t1)/CLOCKS_PER_SEC;
  printf("temps = %f\n", temps);

  //////////////////////////////////////////
  //gmres.setTolerance(0.0000000000001);
  //_sol =gmres.compute(_assbl.GetMatrix()).solve(_assbl.GetSourceTerm2());
  //std::cout << "GMRES:    #iterations: " << gmres.iterations() << ", estimated error: " << gmres.error() << std::endl;
  //_sol =bicg.compute(_assbl.GetMatrix()).solve(_assbl.GetSourceTerm2());
  /////////////////////////////////////////
  double pond1, pond2;
  pond1 =_sol[_grid.ind(_grid.getNbPoints(0)/2-1,_grid.getNbPoints(1)/2-1)];
  pond2 = exp( pow(_x[_grid.getNbPoints(0)/2-1],2)+pow(_y[_grid.getNbPoints(1)/2-1],2));

  for(int i=0;i<_grid.getNbPoints(0)*_grid.getNbPoints(1) + _grid.Get_nbsigne() + _grid.getNbPoints(2);i++){
    _sol[i] =_sol[i] - pond1 + pond2 ;
  }
  ///////////////////////////////////////

  _dxsol.resize(_grid.getNbPoints(0), _grid.getNbPoints(1));

    for(unsigned short i=0;i<((unsigned short)(_grid.getNbPoints(0)));i++){
      for(unsigned short j=0;j<((unsigned short)(_grid.getNbPoints(1)));j++){

        if (_grid.Levelset(i,j) <= 0) {

           uup = _sol[_grid.ind(i+1,j)-1];
           xp = _x[i+1];
           if ((_grid.get_tx2(i,j)) != 0) {
              uup = _sol[_grid.get_tx2(i,j)-1];
              xp = _grid.get_xinter(i,j);
           }


           uum = _sol[_grid.ind(i-1,j)-1];
           xm = _x[i-1];
           if ((_grid.get_tx2(i-1,j)) != 0) {
              uum = _sol[_grid.get_tx2(i-1,j)-1];
              xm = _grid.get_xinter(i-1,j);
           }

           _dxsol(i,j)=(uup-uum)/(xp-xm);


       }

    }
   }


  /////////////////////////////////////////
  for(int i=0; i<_grid.getNbPoints(0)*_grid.getNbPoints(1) + _grid.Get_nbsigne() + _grid.getNbPoints(2); i++){
    residu[i] = 0;
  }
  residu = _assbl.GetMatrix()*_sol - _assbl.GetSourceTerm2();

  //for(int i=0; i< _grid.getNbPoints(2); i++){
  //std::cout<<residu(_grid.Get_nbsigne() +i)<<std::endl;
  //}
  errmax = 0;
  errdxmax = 0.;
  for(unsigned short j=0;j<((unsigned short)(_grid.getNbPoints(1)));j++){
    for(unsigned short i=0;i<((unsigned short)(_grid.getNbPoints(0)));i++){

      exactsol = 0.;
      if (_grid.Levelset(i,j) <= 0) exactsol = exp( pow(_y[j],2) + pow(_x[i],2));
      if (_grid.Levelset(i,j) <= 0) {
        dxsolexact = exactsol*2.*_x[i];
        if (errmax < abs((_sol(_grid.ind(i,j)-1)-exactsol))) {
          errmax =abs((_sol(_grid.ind(i,j)-1)-exactsol));
        }

        if (errdxmax < abs(_dxsol(i,j)-dxsolexact)) {
              errdxmax = abs(_dxsol(i,j)-dxsolexact);
              //std::cout<<errdxmax<<std::endl;
           }
      }

    }
  }

  resmax=0.;
  for(unsigned short i=0;i<((unsigned short)(_grid.getNbPoints(0)));i++){
    for(unsigned short j=0;j<((unsigned short)(_grid.getNbPoints(1)));j++){
      if (_grid.Levelset(i,j) < 0.) {
        if (resmax < abs(residu(_grid.ind(i,j)-1))) {
          resmax = abs(residu(_grid.ind(i,j)-1));
        }
      }
    }
  }

  std::cout<< " " <<std::endl;
  std::cout<< " " <<std::endl;
  std::cout<<"errmax:"<< " " <<std::setprecision(17)<<_grid.getSpacing(0)<<"   "<<errmax<<"   "<<std::setprecision(20)<<_grid.getNbPoints(0)*errmax<<std::endl;
  std::cout<<"errdxmax:"<< " " <<std::setprecision(17)<<errdxmax<<"   "<<std::setprecision(20)<<_grid.getNbPoints(0)*errdxmax<<std::endl;
  std::cout<<"résidus:"<< " " <<std::setprecision(20)<<resmax<<std::endl;


  _writer.write(_sol);
  
_assbl.CLEAR();



}
