#include "grid.hpp"
#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <cmath>
#include "Eigen/Eigen/Eigen"
#include "Eigen/Eigen/Dense"
#include "Eigen/Eigen/Sparse"

using namespace std;
using namespace Eigen;


Grid::Grid (Parameters& p) {

  _N[0] = p.getInt("Nx");
  _N[1] = p.getInt("Ny");
  _N[2] = p.getInt("Ne");

  // Check
  if (_N[0] < 3 or _N[1] <3 or _N[2] <1) {
    std::cerr << "Grid : Insufficient or negative number of points/ number of electrodes  : x " << _N[0] << " y " << _N[1]<< "Ne"<< _N[2] << std::endl;
    abort();
  }

  _h[0] = 2./(long double)(_N[0]);
  _h[1] = 2./(long double)(_N[1]);

  _x.resize(_N[0]); // (xmin+h, ..., xmax-h)
  for (unsigned short i = 0 ; i <((unsigned short)(_N[0])) ; ++i)
  _x[i] =  ((i+1)-0.5)*_h[0]-1.;

  _y.resize(_N[1]);
  for (unsigned short i = 0 ; i < ((unsigned short)(_N[1])) ; ++i)
  _y[i] =  ((i+1)-0.5)*_h[1]-1.;
}

long double Grid::getSpacing (uint dim) {//dimension.

  if (dim!=0 and dim!=1) {
    std::cerr << "Grid::getSpacing: wrong dimension " << dim << std::endl;
    abort();
  }
  return _h[dim];

}

uint Grid::getNbPoints (uint dim) {

  if (dim!=0 and dim!=1 and dim!=2) {
    std::cerr << "Grid::getNbPoints: wrong dimension " << dim << std::endl;
    abort();
  }
  return _N[dim];

}


void Grid::index2D(uint row, uint& i, uint& j) {

  if (row>=_N[0]*_N[1]) {
    std::cerr << "Grid::index2D: given row index (" << row << ")" << " is not in matrix of size " << _N[0]*_N[1] <<  std::endl;
    abort();
  }
  j = row/_N[0];
  i = row%_N[0];

}

void Grid::coords(uint i, uint j, double& x, double& y) {
  x = i*_h[0];
  y = j*_h[1];
}

long double Grid::Levelset(uint i,  uint j)
{
  double yc(0.),xc(0.),r;
  double r0(0.5);

  double xx,yy;


  xx = ((float(i)+1)-0.5)*_h[0]-1.;
  yy = ((float(j)+1)-0.5)*_h[1]-1.;

  r = sqrt(pow((xx-xc),2) + pow((yy-yc),2));

  return (r-r0);

}

double Grid::coef(double x ,  double y)
{
  double c;
  if ((pow(x,2)+pow(y,2)) <= 0.06){
          c = 2;
  } else{
          c =1;
  }

  return 1;
}

void Grid::BuildT()
{

  _tx2.resize(_N[0],_N[1]);
  _ty2.resize(_N[0],_N[1]);
  _xix2_2.resize(_N[0],_N[1]);
  _xiy2_2.resize(_N[0],_N[1]);
  _nbsigne = 0;

  for (unsigned short j=0; j<((unsigned short)(_N[1])); j++)
  {
    for ( unsigned short i=0; i<((unsigned short)(_N[0])); i++){
     _xix2_2(i,j) = 0;
     _xiy2_2(i,j) = 0;
     _tx2(i,j) = 0;
     _ty2(i,j) = 0;
    }
  }

  for (unsigned short j=0; j<((unsigned short)(_N[1])); j++)
  {
    for ( unsigned short i=1; i<((unsigned short)(_N[0]-1)); i++){


          if (Levelset(i,j)*Levelset(i+1,j) <= 0.){
               _xix2_2(i,j) = 1;
               _nbsigne = _nbsigne + 1;
               _tx2(i,j) = _nbsigne;
            }
            else {
               _xix2_2(i,j) = 0;
            }

         }
       }

  for (unsigned short j=1; j<((unsigned short)(_N[1]-1)); j++)
    {
      for (unsigned short i=0; i<((unsigned short)(_N[0])); i++){


          if (Levelset(i,j)*Levelset(i,j+1)<=0.){
             _xiy2_2(i,j) = 1;
             _nbsigne = _nbsigne + 1;
             _ty2(i,j) = _nbsigne;
          }
          else{
             _xiy2_2(i,j) = 0;
          }

       }
     }

}

uint Grid::ind(uint i, uint j) {

  if ( i>=_N[0] or j>=_N[1]) {
    std::cerr << "Grid::index1D: given i,j " << i << "," << j << " but grid has size " << _N[0] << "," << _N[1] << std::endl;
    abort();
  }
  return i*_N[1] + (j+1)  + _N[2] + _nbsigne;
}


double Grid::Funczone( double thetha)
{

  double theta0, pi, dtheta;
  double funczone;

  pi = acos(-1.);
  dtheta =pi/(2*_N[2]);
  funczone = 0;
   for (unsigned short i=0; i<((unsigned short)(_N[2])); i++) {
     theta0  = (i)*2.*pi/_N[2];
     if (abs(thetha-theta0) <= dtheta) {
        funczone = i+1;
     }
 }
    theta0   = 2.*pi;
    if (abs(thetha-theta0) <= dtheta) funczone = 1;
    //cout<< funczone<<endl;
 return funczone;
}


void Grid::DLevelset()
{
  _u.resize(_N[0],_N[1]);
  _v.resize(_N[0],_N[1]);


  double _norme(0);

  for (unsigned short j=0; j<((unsigned short)(_N[1])); ++j)
  {
    for (unsigned short i=0; i<((unsigned short)(_N[0])); ++i){
      _u(i,j)=0;
      _v(i,j)=0;
    }
  }

  for (unsigned short i=1; i<((unsigned short)(_N[0]-1)); ++i)
  {
    for (unsigned short j=1; j<((unsigned short)(_N[1]-1)); ++j){

    _u(i,j) = (Levelset(i+1,j)-Levelset(i-1,j))/(2.*_h[0]);
    _v(i,j) = (Levelset(i,j+1)-Levelset(i,j-1))/(2.*_h[1]);
    _norme = sqrt( pow(_u(i,j),2.) + pow(_v(i,j),2.));
    _u(i,j) =  _u(i,j)/_norme;
    _v(i,j) =  _v(i,j)/_norme;

     }
  }

  for (unsigned short j=1;  j<((unsigned short)(_N[1]-1)); ++j){
  _u(0,j) = (Levelset(1,j)-Levelset(0,j))/(_h[0]);
  _v(0,j) = (Levelset(0,j+1)-Levelset(0,j-1))/(2.*_h[1]);

  _u(_N[0]-1,j) = (Levelset(_N[0]-1,j)-Levelset(_N[0]-2.,j))/(_h[0]);
  _v(_N[0]-1,j) = (Levelset(_N[0]-1,j+1)-Levelset(_N[0]-1,j-1))/(2.*_h[1]);
  }

  for (unsigned short i=1; i<((unsigned short)(_N[0]-1)); ++i){
  _u(i,0) = (Levelset(i+1,0)-Levelset(i-1,0))/(2.*_h[0]);
  _v(i,0) = (Levelset(i,1)-Levelset(i,0))/(_h[1]);

  _u(i,_N[1]-1) = (Levelset(i+1,_N[1]-1)-Levelset(i-1,_N[1]-1))/(2.*_h[0]);
  _v(i,_N[1]-1) = (Levelset(i,_N[1]-1)-Levelset(i,_N[1]-2))/(_h[1]);
  }

  _u(0,0) = (Levelset(1,0)-Levelset(0,0))/(_h[0]);
  _v(0,0) = (Levelset(0,1)-Levelset(0,0))/(_h[1]);

  _u(0,_N[1]-1) = (Levelset(1,_N[1]-1)-Levelset(0,_N[1]-1))/(_h[0]);
  _v(0,_N[1]-1) = (Levelset(0,_N[1]-1)-Levelset(0,_N[1]-2))/(_h[1]);

  _u(_N[0]-1,0) = (Levelset(_N[0]-1,0)-Levelset(_N[0]-2,0))/(_h[0]);
  _v(_N[0]-1,0) = (Levelset(_N[0]-1,1)-Levelset(_N[0]-1,0))/(_h[1]);

  _u(_N[0]-1,_N[1]-1) = (Levelset(_N[0]-1,_N[1]-1)-Levelset(_N[0]-2,_N[1]-1))/(_h[0]);
  _v(_N[0]-1,_N[1]-1) = (Levelset(_N[0]-1,_N[1]-1)-Levelset(_N[0]-1,_N[1]-2))/(_h[1]);

}



 double Grid::Delta(int i, int j)
{


  double  term;
  double epsilo(0.0000001);


     _dxphiplus = (Levelset(i+1,j)-Levelset(i,j))/_h[0];
     _dxphimoins = (Levelset(i,j)-Levelset(i-1,j))/_h[0];
     _dxphizero = (Levelset(i+1,j)-Levelset(i-1,j))/(2.*_h[0]);

     _dyphiplus = (Levelset(i,j+1)-Levelset(i,j))/_h[1];
     _dyphimoins = (Levelset(i,j)-Levelset(i,j-1))/_h[1];
     _dyphizero = (Levelset(i,j+1)-Levelset(i,j-1))/(2.*_h[1]);

     _absgrad = sqrt(_dxphizero*_dxphizero + _dyphizero*_dyphizero + epsilo);

     _delta = 0.;
     if (Levelset(i,j)*Levelset(i+1,j)<=0.) {
          term = abs(Levelset(i+1,j)*_dxphizero)/(_h[0]*_h[0]*abs(_dxphiplus*_absgrad));
          _delta = _delta + term;
          //cout<<setprecision(17)<< _dxphiplus
     }

     if (Levelset(i,j)*Levelset(i-1,j)< 0.) {
        term = abs(Levelset(i-1,j)*_dxphizero)/(_h[0]*_h[0]*abs(_dxphimoins*_absgrad));
        _delta = _delta + term;
     }

    if (Levelset(i,j)*Levelset(i,j+1)<=0.) {
        term = abs(Levelset(i,j+1)*_dyphizero)/(_h[1]*_h[1]*abs(_dyphiplus*_absgrad));
        _delta = _delta + term;
     }

    if (Levelset(i,j)*Levelset(i,j-1) < 0.) {
        term = abs(Levelset(i,j-1)*_dyphizero)/(_h[1]*_h[1]*abs(_dyphimoins*_absgrad));
        _delta = _delta + term;
    }


 return _delta;

}




void Grid::inter()
{
xinter.resize(_N[0],_N[1]);
yinter.resize(_N[0],_N[1]);

double xx,yy,mid,fin,deb,theta,r1,s;
double F,FF;
double yc(0.),xc(0.);


for (unsigned short i=0; i<((unsigned short)(_N[0])); ++i)
{
 for (unsigned short j=0; j<((unsigned short)(_N[1])); ++j){

   xinter(i,j)=0;
   yinter(i,j)=0;

 }
}

s= 0.;
for (unsigned short i=0; i<((unsigned short)(_N[0]-1)); ++i)
{
 for (unsigned short j=0; j<((unsigned short)(_N[1]-1)); ++j){

//calcul pour l'axe x
if  (_xix2_2(i,j) == 1) {
  // dichotomie
  // initial guess
   deb = _x[i];
   fin = _x[i+1];

  for (int n=0; n<9000; ++n){
  mid = (deb+fin)/2.;
  yy = _y[j];
  xx = deb;
  r1 = sqrt(pow((yy-yc),2)+pow((xx-xc),2));
  theta = asin((yy-yc)/r1);
  F = sqrt( pow((xx-xc),2) + pow((yy-yc),2)) - 0.5;
  xx = mid;
  r1 = sqrt(pow((yy-yc),2))+pow((xx-xc),2);
  theta = asin((yy-yc)/r1);
  FF = sqrt( pow((xx-xc),2) + pow((yy-yc),2)) - 0.5;

  if (F*FF < 0.) {
    fin = mid;
  }else {
  deb = mid;
  }
  }
  xinter(i,j) = mid;


  if (abs((xinter(i,j)-_x[i])/_h[0]) <= 0.00000001){
   xinter(i,j) =  (float(i)+1)*_h[0]-1.;
  // s= s+1;
  // cout<<"hello"<<endl;
  }
  if (abs((xinter(i,j)-_x[i+1])/_h[0]) <= 0.00000001){
  xinter(i,j) =  (float(i)+1)*_h[0]-1.;
  //  s = s+1;
  // cout<<"hello"<<endl;
  }
 }

if  (_xiy2_2(i,j) == 1){
// dichotomie
// initial guess
   deb = _y[j];
   fin = _y[j+1];
   for (int n=0; n<9000; ++n){
    mid = (deb+fin)/2.;
    xx = _x[i] ;   //cas du cercle
    if (_y[j] >= yc){
     yinter(i,j) = yc + sqrt(pow(1,2)-pow((_x[i]-xc),2));//voir cette histoire
    }else{
     yinter(i,j) = yc - sqrt(pow(1,2)-pow((_x[i]-xc),2));
    }
    yy = deb;
    r1 = sqrt(pow((yy-yc),2)+pow((xx-xc),2));
    theta = asin((yy-yc)/r1);
    F = sqrt( pow((xx-xc),2) + pow((yy-yc),2)) - 0.5;

    yy = mid;
    r1 = sqrt(pow((yy-yc),2)+pow((xx-xc),2));
    theta = asin((yy-yc)/r1);
    FF = sqrt(pow((yy-yc),2)+pow((xx-xc),2)) -0.5;

    if (F*FF <= 0.){
      fin = mid;
     }else{
    deb = mid;
     }
    }
    yinter(i,j) = mid;

    if (abs((yinter(i,j)-_y[j])/_h[1]) <=0.00000001){
        yinter(i,j) =  (float(j)+1)*_h[1]-1.;
      //  s = s+1;
      //  cout<<"hello"<<endl;
    }
    if (abs((yinter(i,j)-_y[j+1])/_h[1])<=0.00000001){
      yinter(i,j) =  (float(j)+1)*_h[1]-1.;
    //  s = s+1;
    //  cout<<"hello"<<endl;
    }

   }
 }
}

//cout << s<<endl;

}

void Grid::Zonelectrode()
{

  zone.resize(_nbsigne+1);
  double r2;
  double theta;
  int xc(0), yc(0);
  double pi;
  pi = acos(-1.);


  for (int n=0; n<_nbsigne; n++)
   {
   zone[n]=0;
   }


  for (unsigned short i=0; i<((unsigned short)(_N[0])); i++)
  {
    for (unsigned short j=0; j<((unsigned short)(_N[1])); j++){


      if (_tx2(i,j)!= 0){
         r2 = sqrt(pow((xinter(i,j)-xc),2) + pow((_y[j]-yc),2));
         theta = acos( (xinter(i,j)-xc)/r2);
         if (_y[j] <= 0.){
           theta = 2.*pi-theta;
         }
         zone[_tx2(i,j)] = Funczone(theta);
      }

      if (_ty2(i,j) != 0){
         r2 = sqrt(pow((_x[i]-xc),2) + pow((yinter(i,j)-yc),2));
         theta = acos( (_x[i]-xc)/r2);
         if (yinter(i,j) <= 0.) theta = 2.*pi-theta;
         zone[_ty2(i,j)] = Funczone(theta);
      }

      }
    }

}
