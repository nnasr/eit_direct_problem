#ifndef _ASSEMBLER_HPP_
#define _ASSEMBLER_HPP_

//#include "Dense"
//#include "Sparse"
#include "grid.hpp"
#include "Eigen/Eigen/Dense"
#include "Eigen/Eigen/Eigen"
#include "Eigen/Eigen/Sparse"
#include <iostream>

using namespace Eigen;

class HeatProblem;

class HeatAssembler {

  public :

    HeatAssembler(){};
    HeatAssembler(Grid* g, HeatProblem*);

    //Fills the matrix if the interface point is  not on an electode
    void fluxx(int i, int j);
    //Fills the matrix if the interface point is  not on an electode
    void fluxy(int i, int j);
    //Fills the matrix if the interface point is on an electrode
    void electrodefluxx(int i, int j);
    //Fills the matrix if the interface point is on an electrode
    void electrodefluxy(int i, int j);
    //assembles laplacien Matrix
    void BuildLaplacianMatrix();
    // Construit le terme source
	  void BuildSourceTerm();
    void BuildSourceTerm2();
    //builds the exact solution
    void assemblesolexact();
    //Clearing function
    void CLEAR();


    // Pour récuperer la matrice du laplacien
	  Eigen::SparseMatrix<double> GetMatrix(){return _lap_mat;};
	  // Pour récupérer le terme source
	  Eigen::VectorXd  GetSourceTerm(){return _source_term;};
    Eigen::VectorXd  GetSourceTerm2(){return _source_term2;};




  private:

    HeatProblem* _pb; // Source term function
    Grid*        _grid;


    Eigen::SparseMatrix<double> _lap_mat;
  	Eigen::VectorXd _source_term, _source_term2 ;

    typedef Triplet<double> Trip;
    std::vector<Trip> trp;
    Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> dsigma, lap_dir;
    //vector<Triplet<double>> triplets;
  	// vecteur x = {xmin + hx, ... , xmax - hx}
  	// vecteur y = {ymin + hy, ... , ymax - hy}
  	Eigen::VectorXd _x, _y;
    //const StorageIndex& Eigen::Triplet<Scalar, StorageIndex>::col()




    Eigen::VectorXd mat3, matt3;
    Eigen::VectorXd mat2, matt2;
    Eigen::VectorXd mat1, matt1,qqq;
    Eigen::VectorXd Im;
    Eigen::VectorXd Um;
    Eigen::VectorXd sigma;
    int Nx,Ny,Ne,n;
    double dx,dy;
    int marqueur,indk,indi;
    double rr, fix, fiy, theta,fiix,fiiy,d,dd;
    double alphak, betak, xi,yi,xk,yk,xj,yj;
    double alphai, betai ,denom ,pi;
    double alphaj, betaj ;
    double r2,the;
    int m;
    int r_0,somme,dthetaa,dth,Lon;
    double coeff;

};

#endif // _ASSEMBLER_HPP_
